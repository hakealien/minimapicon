local minimapicon = {
    index = nil,
    hprgb = {255, 255, 255},
    mprgb = {255, 255, 255},
    enable = Menu.AddOption({".SCRIPTS", "mini icon"}, "mini icon", ""),
    hpsize = Menu.AddOption({".SCRIPTS", "mini icon"}, "hp size", "", 600, 1800, 50),
    mpsize = Menu.AddOption({".SCRIPTS", "mini icon"}, "mp size", "", 200, 600, 50),
    unitsize = Menu.AddOption({".SCRIPTS", "mini icon"}, "otherunit size", "", 400, 1000, 50)
}

function minimapicon.OnUpdate()
    local me = Heroes.GetLocal()
    if not Menu.IsEnabled(minimapicon.enable) or not me then return end
    for index = 1, Heroes.Count() do
        local hero = Heroes.Get(index)
        if hero and Entity.IsAlive(hero) and not Entity.IsSameTeam(me, hero) and not Entity.IsDormant(hero) then
            if Entity.GetHealth(hero) / Entity.GetMaxHealth(hero) < 0.20 then
                minimapicon.hprgb = {204, 0, 0}
            elseif Entity.GetHealth(hero) / Entity.GetMaxHealth(hero) < 0.40 then
                minimapicon.hprgb = {255, 153, 0}
            elseif Entity.GetHealth(hero) / Entity.GetMaxHealth(hero) < 0.60 then
                minimapicon.hprgb = {255, 255, 0}
            elseif Entity.GetHealth(hero) / Entity.GetMaxHealth(hero) < 0.80 then
                minimapicon.hprgb = {0, 102, 34}
            else
                minimapicon.hprgb = {0, 153, 54}
            end
            if NPC.GetMana(hero) / NPC.GetMaxMana(hero) < 0.20 then
                minimapicon.mprgb = {255, 51, 0}
            elseif NPC.GetMana(hero) / NPC.GetMaxMana(hero) < 0.40 then
                minimapicon.mprgb = {51, 0, 128}
            elseif NPC.GetMana(hero) / NPC.GetMaxMana(hero) < 0.60 then
                minimapicon.mprgb = {102, 0, 255}
            elseif NPC.GetMana(hero) / NPC.GetMaxMana(hero) < 0.80 then
                minimapicon.mprgb = {0, 89, 179}
            else
                minimapicon.mprgb = {0, 128, 255}
            end
            if not minimapicon.index then
                MiniMap.AddIconByName(minimapicon.index, "minimap_ping_baseattacked", Entity.GetAbsOrigin(hero), minimapicon.hprgb[1], minimapicon.hprgb[2], minimapicon.hprgb[3], 255, 0.1, Menu.GetValue(minimapicon.hpsize))
                MiniMap.AddIconByName(minimapicon.index, "minimap_ping_baseattacked", Entity.GetAbsOrigin(hero), minimapicon.mprgb[1], minimapicon.mprgb[2], minimapicon.mprgb[3], 255, 0.1, Menu.GetValue(minimapicon.hpsize) + Menu.GetValue(minimapicon.mpsize))
            end
        else
            minimapicon.index = nil
        end
        if hero and Entity.IsAlive(hero) and Entity.IsSameTeam(me, hero) and hero ~= me and not Entity.IsDormant(hero) and NPC.IsVisible(hero) then
            if not minimapicon.index then
                MiniMap.AddIconByName(minimapicon.index, "minimap_ping_baseattacked", Entity.GetAbsOrigin(hero), 102, 102, 153, 255, 0.1, Menu.GetValue(minimapicon.hpsize))
            end
        else
            minimapicon.index = nil
        end
    end
    for i = 1, NPCs.Count() do
        local npc = NPCs.Get(i)
        if npc and Entity.IsAlive(npc) and not Entity.IsDormant(npc) and Entity.GetField(npc, "m_iTaggedAsVisibleByTeam") == 14 and (NPC.IsNeutral(npc) or NPC.IsRoshan(npc) or NPC.GetUnitName(npc) == "npc_dota_observer_wards" or NPC.GetUnitName(npc) == "npc_dota_sentry_wards") then
            if not minimapicon.index then
                MiniMap.AddIconByName(minimapicon.index, "minimap_ping_baseattacked", Entity.GetAbsOrigin(npc), 0, 0, 255, 255, 0.1, Menu.GetValue(minimapicon.hpsize) - Menu.GetValue(minimapicon.mpsize))
            end
        else
            minimapicon.index = nil
        end
        if npc and Entity.IsAlive(npc) and NPC.GetUnitName(npc) == "npc_dota_thinker" then
            if not minimapicon.index then
                if Entity.GetAbsOrigin(npc):Distance(Vector(0.0, 0.0, 0.0)):Length2D() > 1 then
                    MiniMap.AddIconByName(minimapicon.index, "minimap_treant_ult", Entity.GetAbsOrigin(npc), 204, 255, 51, 255, 0.1, Menu.GetValue(minimapicon.unitsize))
                end
            end
        else
            minimapicon.index = nil
        end
    end
    
end

return minimapicon
